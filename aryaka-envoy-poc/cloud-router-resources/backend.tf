terraform {
  backend "gcs" {
    bucket = "aryaka-perf-poc-state-bucket"
    prefix = "cloud-router/state"
  }
}
