variable "project_id" {
  description = "The GCP project to use for integration tests"
  type        = string
}

variable "domain_name" {
  description = "The domain name for the CloudDNS records"
  type        = string
}

variable "nginx_nlb_ip" {
  description = "The Nginx NLB IP"
  type = list(string)
}

variable "total_domains" {
  description = "Total No.of domains"
  type = number
}
