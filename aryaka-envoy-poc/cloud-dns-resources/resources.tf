resource "google_dns_managed_zone" "private-zone" {
   count = var.total_domains
   name        = "${var.domain_name}-${count.index+1}"
   dns_name    = "${var.domain_name}-${count.index+1}.com."
   visibility = "private"
   project     = var.project_id

   private_visibility_config {
     networks {
       network_url = "projects/${var.project_id}/global/networks/${data.terraform_remote_state.vpc_state.outputs.vpc_network}"
     }
   }
 }

 resource "google_dns_record_set" "a" {
   count = var.total_domains
   project = var.project_id
   name    = "${var.domain_name}-${count.index+1}.com."
   type    = "A"
   ttl     = "300"
   managed_zone = google_dns_managed_zone.private-zone[count.index].name
   rrdatas = var.nginx_nlb_ip
 }

resource "google_dns_record_set" "cname" {
   count = var.total_domains
   project = var.project_id
   name    = "www.${var.domain_name}-${count.index+1}.com."
   type    = "CNAME"
   ttl     = "300"
   managed_zone = google_dns_managed_zone.private-zone[count.index].name
   rrdatas = [google_dns_record_set.a[count.index].name]
 }
