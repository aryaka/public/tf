project = "incubation-408916"
subnets = [
  {
    subnet_name           = "aryaka-gke-envoy-subnet"
    subnet_ip             = "10.1.0.0/20"
    subnet_region         = "us-central1"
    subnet_private_access = "true"
  }
]
networkname = "aryaka-gke-envoy-vpc"
secondary_ranges = {
  aryaka-gke-envoy-subnet = [
    {
      range_name    = "for-pods"
      ip_cidr_range = "10.2.0.0/18"
    },
    {
      range_name    = "for-services"
      ip_cidr_range = "10.3.0.0/20"
    }
  ]

}
