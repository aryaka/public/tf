module "network" {
  source           = "terraform-google-modules/network/google"
  version          = "9.0.0"
  network_name     = var.networkname
  project_id       = var.project
  subnets          = var.subnets
  secondary_ranges = var.secondary_ranges
}

resource "google_compute_firewall" "bastion_iap" {
  name          = "bastion-iap-rule"
  project       = var.project
  network       = var.networkname
  source_ranges = ["35.235.240.0/20"]
  direction     = "INGRESS"
  allow {
    protocol = "tcp"
    ports    = ["22", "3389"]
  }
}

