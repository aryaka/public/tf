data "terraform_remote_state" "vpc_state" {
  backend = "gcs"

  config = {
    bucket = "aryaka-perf-poc-state-bucket"
    prefix = "vpc/state"
  }
}
