resource "google_compute_firewall" "access_web" {
  name          = "allow-traffic-to-nginx"
  network       = data.terraform_remote_state.vpc_state.outputs.vpc_network
  source_ranges = ["0.0.0.0/0"]
  direction     = "INGRESS"
  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }
}
