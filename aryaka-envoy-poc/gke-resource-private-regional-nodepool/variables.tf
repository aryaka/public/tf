variable "project_id" {
  type        = string
  description = "Project Id"
}

variable "region" {
  type        = string
  description = "region"
}

variable "enable_binary_authorization" {
  type        = bool
  description = "Binary auth"
  default     = true
}

variable "node_pools" {
  type        = list(map(string))
  description = "Node Pools"
}

variable "remove_default_node_pool" {
  type        = bool
  description = "Remove default node pool"
}

variable "deletion_protection" {
  type        = bool
  description = "Delete Protection"
}

variable "master_authorized_networks" {
  type    = list(object({ cidr_block = string, display_name = string }))
  default = [{ cidr_block = "10.0.0.0/8", display_name = "bastion" }]
}

variable "default_max_pods_per_node" {
  type        = number
  description = "Default Max Pods Per Node"
}

variable "enable_shielded_nodes" {
  type = bool
}
