provider "google" {
  project = "incubation-408916"
  region  = "us-central1"
}

resource "google_compute_instance" "example_instance" {
  name         = "bastion-instance"
  machine_type = "e2-medium"
  zone         = "us-central1-a"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12" # Change this to the desired OS image
    }
  }

  network_interface {
    network    = data.terraform_remote_state.vpc_state.outputs.vpc_network
    subnetwork = data.terraform_remote_state.vpc_state.outputs.subnets
  }
  metadata_startup_script = <<EOF
  # Install and configure tinyproxy
sudo apt update
sudo apt install -y tinyproxy
sudo mkdir -p /var/log/tinyproxy
sudo chown tinyproxy:tinyproxy /var/log/tinyproxy
sudo sed -i '/^Allow /c\\Allow localhost' /etc/tinyproxy/tinyproxy.conf
sudo systemctl restart tinyproxy
EOF
  shielded_instance_config {
    enable_secure_boot = true
  }
}
