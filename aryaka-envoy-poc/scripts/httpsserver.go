package main

import (
	"bufio"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"strings"
	"time"
	"os/exec"
)

const (
	serverRootBasePath = "/var/www/"
	certDirectory      = "/etc/nginx/ssl/"
)

// This function will take baseaddress and total domains as input parameters and generate
// many no.of totaldomains using the format "www.<basedomain>-int.com"
func generateDomains(baseaddress string, totaldomains int) []string {
	domains := make([]string, totaldomains)
	for i := 1; i <= totaldomains; i++ {
		domain := fmt.Sprintf("%s-%d.com", baseaddress, i)
		domains[i-1] = domain
	}
	return domains
}

// This function will create directories for nginx to serve the webpages for each domain created
func createDirectories(domains []string) {
	for _, domain := range domains {
		dirPath := serverRootBasePath + domain
		err := os.MkdirAll(dirPath, 0755)
		if err != nil {
			fmt.Println("Error creating directory", dirPath, ":", err)
			continue // Continue to next domain on error
		}
		fmt.Println("Created server directory:", dirPath)
	}
	for _, domain := range domains {
		dirPath := certDirectory + domain
		err := os.MkdirAll(dirPath, 0755)
		if err != nil {
			fmt.Println("Error creating directory", dirPath, ":", err)
			continue // Continue to next domain on error
		}
		fmt.Println("Created cert directory:", dirPath)
	}


}

func generateCertKey(domain string) (string, string, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return "Please verify the command:", "Server private key can't be generated", err
	}
	//Create cert template for the domain
	certTemplate := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{domain},
			CommonName:   domain,
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(1, 0, 0), // Valid for 1 year
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}
	certTemplate.DNSNames = []string{domain,"www."+domain}
	certDER, err := x509.CreateCertificate(rand.Reader, &certTemplate, &certTemplate, &privateKey.PublicKey, privateKey)
	if err != nil {
		return "", "", err
	}
	// Encode the private key to PEM format
	privateKeyPEM := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privateKey)})
	if privateKeyPEM == nil {
		return "", "", fmt.Errorf("failed to encode private key to PEM for %s", domain)
	}

	// Encode the certificate to PEM format
	caCertPEM := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER})
	if caCertPEM == nil {
		return "", "", fmt.Errorf("failed to encode certificate to PEM for %s", domain)
	}

	return string(privateKeyPEM), string(caCertPEM), nil
}

func generateHTMLcontent(domain string) (string, error) {
	const divContent = `<div>This is a sample content for domain %s to generate a 40KB HTML file.</div>`
	const targetSizeKB = 40
	const targetSizeBytes = targetSizeKB * 1024

	var sb strings.Builder
	for sb.Len() < targetSizeBytes {
		sb.WriteString(fmt.Sprintf(divContent, domain))
	}

	// Trim excess content to precisely reach 40KB
	htmlBytes := []byte(sb.String())
	htmlBytes = htmlBytes[:targetSizeBytes]

	return string(htmlBytes), nil
}

func generateServerBlock(file *os.File, domain string) error {
	sslCertPath := fmt.Sprintf("/etc/nginx/ssl/%s/servercert.pem", domain)
	sslKeyPath := fmt.Sprintf("/etc/nginx/ssl/%s/serverkey.pem", domain)
	rootPath := fmt.Sprintf("/var/www/%s", domain)
	serverName := domain
	if !strings.HasPrefix(domain, "www.") {
		serverName = "www." + domain
	}
	_, err := file.WriteString(fmt.Sprintf(`
server {
    ssl_session_tickets off;
    ssl_session_cache off;
    access_log  off;

    listen 443 ssl;
    server_name %s %s;
    ssl_certificate %s;
    ssl_certificate_key %s;


    location / {
        root %s;
    }
}
`, domain, serverName, sslCertPath, sslKeyPath, rootPath))

	return err
}
func createNginxConf(nginxConfPath, nginxConfContent string) error {
	// Delete existing nginx.conf file if it exists
	if _, err := os.Stat(nginxConfPath); err == nil {
		err := os.Remove(nginxConfPath)
		if err != nil {
			return err
		}
	}

	// Create a new nginx.conf file
	err := ioutil.WriteFile(nginxConfPath, []byte(nginxConfContent), 0644)
	if err != nil {
		return err
	}

	fmt.Println("Nginx configuration file created successfully at", nginxConfPath)
	return nil
}

func main() {
	var totalNoOfDomains int
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your choice of base address to generate domain names:")
	user_input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Error Reading Input")
		return
	}
	user_input = strings.TrimSpace(user_input)
	fmt.Print("Enter the total no of domains:")
	fmt.Scan(&totalNoOfDomains)
	if err != nil {
		fmt.Println("Error reading input:", err)
		return // Or handle the error as needed
	}
	domainslist := generateDomains(user_input, totalNoOfDomains)
	fmt.Println(domainslist)
	createDirectories(domainslist)
	for _, domain := range domainslist { // Loop through the slice of domains
		privateKeyPEM, certPEM, err := generateCertKey(domain)
		if err != nil {
			fmt.Println("Error generating cert/key for", domain, ":", err)
			continue
		}
		privateKeyFile := fmt.Sprintf("/etc/nginx/ssl/%s/serverkey.pem", domain)
		err = os.WriteFile(privateKeyFile, []byte(privateKeyPEM), 0600)
		if err != nil {
			fmt.Println("Error writing private key to file:", err)
			continue
		}
		certFile := fmt.Sprintf("/etc/nginx/ssl/%s/servercert.pem", domain)
		err = os.WriteFile(certFile, []byte(certPEM), 0644)
		if err != nil {
			fmt.Println("Error writing cert to file:", err)
			continue
		}
	}
	for _, domain := range domainslist {
		htmlcontent, err := generateHTMLcontent(domain)
		if err != nil {
			fmt.Println("Error generating HTML content", domain, ":", err)
			continue
		}
		htmlFile := fmt.Sprintf("/var/www/%s/index.html", domain)
		err = os.WriteFile(htmlFile, []byte(htmlcontent), 0755)
		if err != nil {
			fmt.Println("Error writing private key to file:", err)
			continue
		}

	}
	confFilePath := "/etc/nginx/sites-available/default"
	if _, err := os.Stat(confFilePath); err == nil {
		// If the file exists, delete it
		err := os.Remove(confFilePath)
		if err != nil {
			fmt.Println("Error deleting file:", err)
			return
		}
	}
	file, err := os.Create(confFilePath)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()
	for _, domain := range domainslist {
		err := generateServerBlock(file, domain)
		if err != nil {
			fmt.Println("Error generating server block for", domain, ":", err)
			return
		}
	}
	nginxConfPath := "/etc/nginx/nginx.conf"
	nginxConfContent := `user root;
worker_processes auto;
worker_rlimit_nofile 10240;
pid /run/nginx.pid;

events {
        use epoll;
        worker_connections 10240;
        multi_accept off;
        accept_mutex off;
}

http {
       server {
	listen 80 default_server;


	server_name _;

	location / {
		try_files $uri $uri/ =404;
	}

}


	sendfile on;
	keepalive_timeout 25s;
	types_hash_max_size 1000000;


	include /etc/nginx/mime.types;



        default_type application/octet-stream;
		access_log off;





	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}`
	err = createNginxConf(nginxConfPath, nginxConfContent)
	if err != nil {
		fmt.Println("Error modifying nginx.conf:", err)
		return
	}
	cmd := exec.Command("systemctl", "restart", "nginx")
	_, err = cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	outputFileName := "domains.txt"
	data := strings.Join(domainslist,"\n")
	err = ioutil.WriteFile(outputFileName, []byte(data),0644)
        if err != nil {
		fmt.Println("Error writing to file:",err)
		return
        }

}

