provider "google" {
  project = var.project
  region  = "us-central1"
}

resource "google_compute_instance" "nginx_server" {
  name         = "tf-nginx-instance"
  machine_type = "n2-standard-64"
  zone         = "us-central1-a"
  min_cpu_platform = "Intel Ice Lake"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12" # Change this to the desired OS image
    }
  }

  network_interface {
    network    = data.terraform_remote_state.vpc_state.outputs.vpc_network
    subnetwork = data.terraform_remote_state.vpc_state.outputs.subnets
  }
   metadata_startup_script = <<EOF
#!/bin/bash
# Update and install necessary packages
sudo apt update && sudo apt install -y wget

# Install Go 1.19.8
wget https://dl.google.com/go/go1.19.8.linux-amd64.tar.gz
sudo tar -xvf go1.19.8.linux-amd64.tar.gz -C /usr/local
export PATH=$PATH:/usr/local/go/bin
echo "export PATH=\$PATH:/usr/local/go/bin" >> ~/.profile

# Install Nginx
sudo apt install -y nginx

# Enable and start Nginx service
sudo systemctl enable nginx
sudo systemctl start nginx

# Set ulimit for file descriptors
echo "* soft nofile 1000000" | sudo tee -a /etc/security/limits.conf
echo "* hard nofile 1000000" | sudo tee -a /etc/security/limits.conf
echo "root soft nofile 1000000" | sudo tee -a /etc/security/limits.conf
echo "root hard nofile 1000000" | sudo tee -a /etc/security/limits.conf

# Configure sysctl for local port range
echo "net.ipv4.ip_local_port_range = 10000 65535" | sudo tee /etc/sysctl.d/99-local-port-range.conf
sudo sysctl -p /etc/sysctl.d/99-local-port-range.conf

EOF
  shielded_instance_config {
    enable_secure_boot = true
  }
}
