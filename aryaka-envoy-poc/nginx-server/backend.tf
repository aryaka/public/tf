terraform {
  backend "gcs" {
    bucket = "aryaka-perf-poc-state-bucket"
    prefix = "nginx/state"
  }
}
